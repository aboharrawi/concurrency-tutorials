import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static java.lang.Thread.sleep;

public class Join {

    public static void main(String[] args) {
        Thread t = new Thread(new Sleeper(5000), "thread1");
        Thread t2 = new Thread(new Sleeper(1000), "thread2");

        t.start();
        t2.start();

        try {
            t.join();
            t2.join();
        } catch (InterruptedException ignore) {
        } finally {
            System.out.println("Finished at : " + new SimpleDateFormat("ss.SSS", Locale.ENGLISH).format(new Date()));
        }
    }

    static class Sleeper implements Runnable {
        private long millis;

        Sleeper(long millis) {
            this.millis = millis;
        }

        @Override
        public void run() {
            try {
                System.out.println("Sleeping for " + millis + " millis at : " + new SimpleDateFormat("ss.SSS", Locale.ENGLISH).format(new Date()));
                sleep(millis);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

}
