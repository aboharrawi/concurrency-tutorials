import java.util.List;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CyclicBarrierDemo {

    private static CyclicBarrier cyclicBarrier;
    private static final List<List<Integer>> allNumbers = new CopyOnWriteArrayList<>();
    private static final Random random = new Random();

    public static void main(String[] args) {

        cyclicBarrier = new CyclicBarrier(6, new Adder());
        System.out.println("Spawning 6 worker threads to compute 10 partial results each");
        for (int i = 0; i < 6; i++) {
            Thread worker = new Thread(new Generator(10, 1000));
            worker.setName("Thread " + i);
            worker.start();
        }
    }

    static class Generator implements Runnable {
        private int count;
        private long sleep;

        Generator(int count, long sleep) {
            this.count = count;
            this.sleep = sleep;
        }

        @Override
        public void run() {
            String thisThreadName = Thread.currentThread().getName();
            allNumbers.add(Stream.iterate(0, n -> random.nextInt(10))
                    .limit(count)
                    .peek(k -> System.out.println(thisThreadName + ": generated number -> " + k))
                    .collect(Collectors.toList()));
            try {
                printFormat("%s waiting for others to reach barrier. Sleeping for %d millis.", thisThreadName, sleep);
                Thread.sleep(sleep);
                cyclicBarrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }

        }
    }

    static class Adder implements Runnable {

        @Override
        public void run() {
            String thisThreadName = Thread.currentThread().getName();
            System.out.println(thisThreadName + ": Computing final sum.");

            int sum;

            sum = allNumbers.stream()
                    .peek(k -> System.out.print("\nAdding "))
                    .flatMap(List::stream)
                    .mapToInt(Integer::intValue)
                    .peek(k -> System.out.print(k + " "))
                    .sum();


            printFormat("\n%s: Final result = %d", thisThreadName, sum);
        }
    }


    private static void printFormat(String format, Object... args) {
        System.out.println(String.format(format, args));
    }
}